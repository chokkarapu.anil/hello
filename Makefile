PROJECT_NAME ?= hello

clean:
	rm -rf bin/
	rm -rf cover/

mod:
	go mod tidy
	go mod vendor

build: clean mod
	GOOS=linux GOARCH=amd64 go build -o bin/$(PROJECT_NAME)-linux-amd64 ./cmd/$(PROJECT_NAME)
	GOOS=linux GOARCH=386 go build -o bin/$(PROJECT_NAME)-linux-386 ./cmd/$(PROJECT_NAME)
	GOOS=darwin GOARCH=amd64 go build -o bin/$(PROJECT_NAME)-darwin-amd64 ./cmd/$(PROJECT_NAME)
	GOOS=windows GOARCH=amd64 go build -o bin/$(PROJECT_NAME)-window-amd64 ./cmd/$(PROJECT_NAME)
	GOOS=windows GOARCH=386 go build -o bin/$(PROJECT_NAME)-windows-386 ./cmd/$(PROJECT_NAME)

test:
	go test ./... -count 1 -v -timeout 5s

